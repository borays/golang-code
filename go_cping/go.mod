module go_cping

go 1.17

require (
	github.com/go-ping/ping v0.0.0-20211130115550-779d1e919534
	github.com/gookit/color v1.5.0
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/net v0.0.0-20220121210141-e204ce36a2ba // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)
