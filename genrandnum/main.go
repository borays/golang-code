package genrandnum

import (
	"math/rand"
	"time"
)

func genRandNum(min, max int) int {
	rand.Seed(time.Now().UnixMicro()) //随机种子
	if min > max {
		min, max = max, min
		return rand.Intn(max-min) + 1 + min

	} else if min == max {

		return min

	} else {

		return rand.Intn(max-min) + 1 + min
	}

}
