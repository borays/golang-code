package main

import (
	"flag"
	"fmt"
	"net"
	"time"
)

func tcp(url string) {
	_, err := net.DialTimeout("tcp", url, time.Second*2)
	if err != nil {
		fmt.Println("Closed")
	} else {
		fmt.Println("Open")
	}

}

func main() {
	var ip string
	var port string
	flag.StringVar(&ip, "a", "", "ip address for check")
	flag.StringVar(&port, "p", "", "port for check")
	flag.Parse()

	if ip != "" && port != "" {
		ipaddr := ip + ":" + port
		tcp(ipaddr)
	} else {
		flag.Usage()

	}

	// ipaddr := ip + ":" + port
	// tcp(ipaddr)

}
