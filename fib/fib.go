package main

import "fmt"

func main() {
	var i int
	fmt.Printf("请输入范围: ")
	fmt.Scanln(&i)

	var a int = 0
	var b int = 1
	for {
		fmt.Printf("%d ", a)
		a, b = b, a+b
		if a > i {
			break
		}
	}
}
