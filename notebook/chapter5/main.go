package main

import (
	"fmt"
)

func main() {

	// for i := 10; i > 0; i-- {

	randSpeed := genRandSpeed(16, 30)
	days := needDays(randSpeed)
	fmt.Printf("%v km/s, %d days\n", randSpeed, days)

	// }

}
