package main

import (
	"math/rand"
	"time"
)

func genRandSpeed(min, max int) int {
	rand.Seed(time.Now().UnixMicro()) //随机种子
	if min > max {
		min, max = max, min
		return rand.Intn(max-min+1) + min

	} else if min == max {

		return min

	} else {

		return rand.Intn(max-min+1) + min

	}

}

func needDays(speed int) int {
	var disance = 62100000 //km
	return disance / speed / 60 / 60 / 24

}
