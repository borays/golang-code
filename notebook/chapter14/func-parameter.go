package main

import (
	"fmt"
	"time"
)

// type kelvin float64
type sensor func() kelvin

// func measureTemperature(sample int, sensor func() kelvin) {
// for i := 0; i < sample; i++ {
// 	k := sensor()
// 	fmt.Printf("%v k\n", k)
// 	time.Sleep(time.Second)
// }
// }

func measureTemperature(sample int, s sensor) {

	for i := 0; i < sample; i++ {
		// k := s()

		fmt.Printf("%v k\n", s())
		time.Sleep(time.Second)
	}
}
