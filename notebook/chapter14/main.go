package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixMicro()) //随机种子
	sensor := fakeSensor
	fmt.Printf("%T\n %v\n", sensor, sensor())

	sensor = realSensor
	fmt.Printf("%T\n %v\n", sensor, sensor())

	// measureTemperature(3, fakeSensor)
	// measureTemperature(3, realSensor)

	//masquerade
	f()
	ff := func(message string) {
		fmt.Println(message)
	}
	ff("hah")

	func() {
		fmt.Println("anonymous")
	}()

	ss := calibrate(fakeSensor, 5)
	fmt.Println(ss())

}

type kelvin float64

func fakeSensor() kelvin {
	return kelvin(rand.Intn(151) + 150)

}

func realSensor() kelvin {
	return 0
}
