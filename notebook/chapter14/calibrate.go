package main

func calibrate(s sensor, offset kelvin) sensor {
	return func() kelvin {
		return s() + offset

	}
}
