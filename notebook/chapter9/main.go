package main

import (
	"fmt"
)

// "fmt"

func main() {
	fmt.Printf("密文\n")
	message := `As the name suggests, it is a kind of work that translates the meaning of English into another language. It requires translators to have strong language skills and be able to fully express the original meaning of English. This is an activity in which different languages express each other. How to prepare for the 2020 Exam English? The following content excerpts from Kaobo English Translation: In the entire English learning stage, English translation ability is a vital part of the comprehensive ability of English, and it is also one of the basic abilities that must be possessed in english-related occupations in the future, and, in today's global internationalization context, English is a universal language, then English translation is an important bridge to achieve mutual transformation between languages, but the level of English translation is high and low. Taking China and Western countries as an example, in addition to the understanding of Chinese and Western cultures will affect the improvement of English translation ability, the difference between English and Chinese languages will also have a certain impact`
	message2 := `Af gur anzr fhttrfgf, vg vf n xvaq bs jbex gung genafyngrf gur zrnavat bs Eatyvfu vagb nabgure ynathntr. Ig erdhverf genafyngbef gb unir fgebat ynathntr fxvyyf naq or noyr gb shyyl rkcerff gur bevtvany zrnavat bs Eatyvfu. Tuvf vf na npgvivgl va juvpu qvssrerag ynathntrf rkcerff rnpu bgure. Hbj gb cercner sbe gur 2020 Eknz Eatyvfu? Tur sbyybjvat pbagrag rkprecgf sebz Knbob Eatyvfu Tenafyngvba: Ia gur ragver Eatyvfu yrneavat fgntr, Eatyvfu genafyngvba novyvgl vf n ivgny cneg bs gur pbzcerurafvir novyvgl bs Eatyvfu, naq vg vf nyfb bar bs gur onfvp novyvgvrf gung zhfg or cbffrffrq va ratyvfu-eryngrq bpphcngvbaf va gur shgher, naq, va gbqnl'f tybony vagreangvbanyvmngvba pbagrkg, Eatyvfu vf n havirefny ynathntr, gura Eatyvfu genafyngvba vf na vzcbegnag oevqtr gb npuvrir zhghny genafsbezngvba orgjrra ynathntrf, ohg gur yriry bs Eatyvfu genafyngvba vf uvtu naq ybj. Tnxvat Cuvan naq Wrfgrea pbhagevrf nf na rknzcyr, va nqqvgvba gb gur haqrefgnaqvat bs Cuvarfr naq Wrfgrea phygherf jvyy nssrpg gur vzcebirzrag bs Eatyvfu genafyngvba novyvgl, gur qvssrerapr orgjrra Eatyvfu naq Cuvarfr ynathntrf jvyy nyfb unir n pregnva vzcnpg`

	rot13(message)
	fmt.Printf("\n原文\n")
	rot13(message2)

}
