package main

import (
	"fmt"
)

func testSwitch() {

	var command = "go outside"
	switch command {
	case "go outside":
		fmt.Println("选择了go outside")
	case "go inside":
		fmt.Println("选择了go inside")
	case "read":
		fmt.Println("选择了read")
	default:
		fmt.Println("未匹配打印默认内容")

	}
}

func testSwitch2() {
	var room = "704"
	switch {
	case room == "1404":
		fmt.Println("1404")
	case room == "704":
		fmt.Println("704")
		fallthrough
	case room == "804":
		fmt.Println("804")

	}
}
