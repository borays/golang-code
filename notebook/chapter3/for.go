package main

import (
	"fmt"
	"math/rand"
	"time"
)

func testfor1() {

	var count = 10
	for count > 0 {

		fmt.Println(count)

		time.Sleep(time.Second)
		count--

	}

	fmt.Println("finished")
}

func testfor2() {
	var degress = 0
	for {
		fmt.Println(degress)
		degress++

		if degress >= 360 {
			degress = 0
			if rand.Intn(2) == 0 {
				break
			}
		}
	}
}

func testfor3() {
	var count = 10
	rand.Seed(time.Now().Unix())
	for count > 0 {
		fmt.Println(count)
		time.Sleep(time.Second)

		if rand.Intn(100) == 0 {
			break

		}
		count--
	}
	if count == 0 {
		fmt.Println("Success")
	} else {
		fmt.Println("Faild")
	}
}
