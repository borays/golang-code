package main

import (
	"fmt"
	"math/rand"
	"time"
)

func guessNum() {
	fmt.Printf("Please input an number(1-100): ")
	var num int
	fmt.Scanln(&num)

	var times int
	rand.Seed(time.Now().Unix())
	for {

		var autoguess int = rand.Intn(100)
		// time.Sleep(time.Second)
		if autoguess == num {
			fmt.Println("you guessed!")
			break
		} else if autoguess > num {
			fmt.Printf("try again, you guess %v is to large than %v.\n", autoguess, num)
		} else {
			fmt.Printf("try again, you guess %v is to small than %v.\n", autoguess, num)
		}
		times++
	}
	fmt.Printf("you tried %v times", times)
}
