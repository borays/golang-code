package main

import (
	"fmt"
)

type celsius float64
type kelvin float64
type fahrenheit float64

func main() {

	// type fahrenheit float64
	// var temperature celsius = 20
	// const degree = 20
	// temperature += degree

	// var testfloat64err float64 = 10
	// // temperature += testfloat64err
	// temperature += celsius(testfloat64err)
	// fmt.Printf("%T,%[1]v", temperature)

	// var k kelvin = 294
	// c := kelvinToCelsius(k)
	// fmt.Printf("%v\n", c)

	var c celsius = 127
	var k kelvin = 294
	var f fahrenheit = 90
	// k := celsiusToKelvin(c)
	// fmt.Printf("%v\n", k)
	fmt.Println(c.kelvin())
	fmt.Println(k.celsius())
	fmt.Println(f.celsius())

}

//函数
func kelvinToCelsius(k kelvin) celsius {
	return celsius(k - 273.15)
}

//方法
func (k kelvin) celsius() celsius {
	return celsius(k - 273.15)
}

//函数
func celsiusToKelvin(c celsius) kelvin {
	return kelvin(c + 273.15)
}

//方法
func (c celsius) kelvin() kelvin {
	return kelvin(c + 273.15)

}

//方法
func (f fahrenheit) celsius() celsius {
	return celsius((f - 32) * 5 / 9.0)

}
