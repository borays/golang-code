package main

import (
	"fmt"
	"math/rand"
	"time"
)

func isLeap(year int) bool {

	if year%400 == 0 || (year%4 == 0 && year%100 != 0) {
		return true
	} else {
		return false
	}

}

// func genRandRand(min, max int64) int64 {
// 	if min > max {
// 		panic("the min is greater than max!")
// 	}

// 	if min < 0 {
// 		f64Min := math.Abs(float64(min))
// 		i64Min := int64(f64Min)
// 		result, _ := rand.Int(rand.Reader, big.NewInt(max+1+i64Min))

// 		return result.Int64() - i64Min
// 	} else {
// 		result, _ := rand.Int(rand.Reader, big.NewInt(max-min+1))
// 		return min + result.Int64()
// 	}
// }

func genRandNum(min, max int) int {
	rand.Seed(time.Now().UnixMicro()) //随机种子
	if min > max {
		min, max = max, min
		return rand.Intn(max-min) + 1 + min

	} else {

		return rand.Intn(max-min) + 1 + min
	}

}

func formatDateTime() {

	var era = "AD"
	year := genRandNum(1800, 2022)
	month := genRandNum(1, 12)

	var daysPerMonth int = 31
	switch month {
	case 4, 6, 10, 11:
		daysPerMonth = 30
	case 2:
		if isLeap(year) {
			daysPerMonth = 29
		} else {
			daysPerMonth = 28
		}
	}
	day := genRandNum(1, daysPerMonth)
	// fmt.Println(era, year, isLeap(year), month, day)
	fmt.Printf("%v %v-%v-%v isLeap:%v\n", era, year, month, day, isLeap(year))
}
