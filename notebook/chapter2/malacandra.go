package main

import (
	"fmt"
)

func calcSpeed() {
	var distance = 56000000
	var days = 28
	var distancePerDay = distance / days
	fmt.Printf("Speed: %v", distancePerDay/24)
}
