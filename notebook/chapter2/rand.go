package main

import (
	"math/rand"
)

func randNum(n int) int {
	return rand.Intn(n) + 1
}
